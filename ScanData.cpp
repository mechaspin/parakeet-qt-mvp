#include "ScanData.h"

ScanData::ScanData(QObject *parent) : QObject(parent)
{

}

ScanData::ScanData(const ScanData &src)
{
    this->from = src.from;
    this->span = src.span;
    this->count = src.count;
    this->reserved = src.reserved;
    for(int i = 0; i < 1000; i++)
    {
        this->dist[i] = src.dist[i];
        this->intensity[i] = src.intensity[i];
    }
}

ScanData &ScanData::operator=(const ScanData &src)
{
    this->from = src.from;
    this->span = src.span;
    this->count = src.count;
    this->reserved = src.reserved;
    for(int i = 0; i < 1000; i++)
    {
        this->dist[i] = src.dist[i];
        this->intensity[i] = src.intensity[i];
    }
    return *this;
}
