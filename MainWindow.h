#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include "SensorInterface.h"
#include <QMap>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_connectButton_clicked();
    void processScan(ScanData &scan);
    void updateScene();
    void on_checkBoxSmoothing_clicked(bool checked);
    void on_checkBoxDragPoints_clicked(bool checked);

private:
    Ui::MainWindow *ui;
    SensorInterface interface;
    QGraphicsScene *scene;
    QMap<quint16, ScanData> scans;
};
#endif // MAINWINDOW_H
