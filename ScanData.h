#ifndef SCANDATA_H
#define SCANDATA_H

#include <QObject>

class ScanData : public QObject
{
    Q_OBJECT
public:
    explicit ScanData(QObject *parent = nullptr);
    ScanData(const ScanData &src);
    ScanData &operator=(const ScanData &src);
    unsigned short from; // Starting angle, unit （0.1°)
    unsigned short span; // Span angle, unit (0.1°)
    unsigned short count; // Points
    unsigned short reserved;
    unsigned short dist[1000]; // distance
    unsigned char intensity[1000]; // intensity

signals:

};

#endif // SCANDATA_H
