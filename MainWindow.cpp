#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QDebug>
#include <QInputDialog>
#include <QtMath>
#include <QTimer>
#include <QCloseEvent>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),
      interface(parent)
{
    ui->setupUi(this);

    scene = new QGraphicsScene(0, 0, 1000, 1000);
    scene->setBackgroundBrush(QBrush(Qt::gray));

    ui->graphicsView->setScene(scene);

    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &MainWindow::updateScene);
    timer->start(100);

    connect(&interface, &SensorInterface::newScan, this, &MainWindow::processScan);
}

MainWindow::~MainWindow()
{
    interface.stop();
    delete ui;
}

void MainWindow::on_connectButton_clicked()
{
    QList<QSerialPortInfo> ports = interface.getSerialPorts();
    QStringList portNames;
    for(QSerialPortInfo &info : ports)
    {
        portNames << info.portName() + ": " + info.description();
//        qDebug() << info.productIdentifier();
//        qDebug() << info.description();
    }

    bool ok;
    QString item = QInputDialog::getItem(this, "Select COM Port", "Available Ports: ", portNames, 0, false, &ok);
    if(!ok)
    {
        return;
    }
    ui->connectButton->setText("Connecting...");
    QApplication::processEvents();

    QString portName = item.split(":").at(0);

    bool connected = interface.initialize(portName, 500000);
    if(connected)
    {
        interface.start();
        ui->connectButton->setChecked(true);
        ui->connectButton->setText("Connected");
    }
}

void MainWindow::processScan(ScanData &scan)
{
    scans.insert(scan.from, scan);
}

void MainWindow::updateScene()
{
    scene->clear();

    QPen pen;  // creates a default pen
    pen.setWidth(1);
    pen.setBrush(Qt::black);

    // Calculate Scale Factor
    // 10: Range Parameter
    double range = ui->comboBox->currentText().toDouble();
    double scaleFactor = 1000 / range;

    double centerX = scene->width() / 2;
    double centerY = scene->height() / 2;

    // Draw 10 Circles
    for(int i = 10; i > 0; i--)
    {
        double radius = scene->width()/10 * i;
        double x = centerX - radius/2;
        double y = centerY - radius/2;
        scene->addEllipse(x, y, radius, radius, pen);
    }

    // Draw Lines
    scene->addLine(centerX, 0, centerX, scene->height());
    scene->addLine(0, centerY, scene->width(), centerY);

    // Draw Points
    QMapIterator<quint16, ScanData> itr(scans);
    while (itr.hasNext())
    {
        itr.next();
        ScanData scan = itr.value();

//        qDebug() << "start:" << scan.from;
//        qDebug() << " span:" << scan.span;
//        qDebug() << "count:" << scan.count;

        double deltaAngle_deg = scan.span / 10.0 / scan.count;
//        qDebug() << "dAngle: " << deltaAngle_deg;

        for(int i = 0; i < scan.count; i++)
        {
            double pointX_m = 0;
            double pointY_m = 0;
            double distance_m = scan.dist[i] / 1000.0;
            double angle_deg = scan.from / 10.0 + (deltaAngle_deg * i);

//            qDebug() << "Point: " << angle_deg << ", " << distance_m;
            pointX_m = distance_m * std::cos(qDegreesToRadians(angle_deg));
            pointY_m = distance_m * std::sin(qDegreesToRadians(angle_deg));

            double pointSceneX = centerX + pointX_m * scaleFactor;
            double pointSceneY = centerY + pointY_m * scaleFactor;

            scene->addEllipse(pointSceneX-3, pointSceneY-3, 3, 3, Qt::NoPen, QBrush(Qt::blue));
        }
    }

    ui->graphicsView->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
}

void MainWindow::on_checkBoxSmoothing_clicked(bool checked)
{
    interface.setDataSmoothing(checked);
}

void MainWindow::on_checkBoxDragPoints_clicked(bool checked)
{
    interface.setDragPointRemoval(checked);
}

