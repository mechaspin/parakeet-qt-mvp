#include "SensorInterface.h"
#include <QtSerialPort/QSerialPortInfo>
#include <QDebug>
#include <QMessageBox>
#include <ScanData.h>
#include <QElapsedTimer>

SensorInterface::SensorInterface(QObject *parent) :
    QObject(parent),
    intensityMode(false),
    connected(false)
{
    connect(&serialPort, &QSerialPort::readyRead, this, &SensorInterface::readData);
}

QList<QSerialPortInfo> SensorInterface::getSerialPorts()
{
    return QSerialPortInfo::availablePorts();
}

bool SensorInterface::initialize(QString port, int baud)
{
    serialPort.setPortName(port);
    serialPort.setBaudRate(baud);
    serialPort.setDataBits(QSerialPort::Data8);
    serialPort.setParity(QSerialPort::NoParity);
    serialPort.setStopBits(QSerialPort::OneStop);
    serialPort.setFlowControl(QSerialPort::NoFlowControl);
    if (serialPort.open(QIODevice::ReadWrite))
    {
    }
    else
    {
        qDebug() << "Error: " << serialPort.error();
        return false;
    }

    connected = true;

    // STOP
    serialPort.write("LSTOPH");
    if (serialPort.waitForReadyRead(1000))
    {
        QByteArray response = serialPort.readAll();
//        qDebug() << response;
    }

    // Set Intensity Mode OFF
    intensityMode = false;
    serialPort.write("LNCONH");
    if (serialPort.waitForReadyRead(1000))
    {
        QByteArray response = serialPort.readAll();
//        qDebug() << response;
    }

    // Set RPM to 600
    serialPort.write("LSRPM:600H");
    if (serialPort.waitForReadyRead(1000))
    {
        QByteArray response = serialPort.readAll();
//        qDebug() << response;
    }

    // Enable Data Smoothing
    this->setDataSmoothing(true);
    if (serialPort.waitForReadyRead(1000))
    {
        QByteArray response = serialPort.readAll();
//        qDebug() << response;
    }

    // Enable Drag Point Removal
    this->setDragPointRemoval(true);
    if (serialPort.waitForReadyRead(1000))
    {
        QByteArray response = serialPort.readAll();
//        qDebug() << response;
    }


    return true;
}

void SensorInterface::start()
{
    if(!connected)
    {
        return;
    }

    serialPort.write("LSTARH");
    if (serialPort.waitForReadyRead(1000))
    {
        QByteArray response = serialPort.readAll();
//        qDebug() << response;
    }
}

void SensorInterface::stop()
{
    if(!connected)
    {
        return;
    }
    serialPort.write("LSTOPH");
    if (serialPort.waitForReadyRead(1000))
    {
        QByteArray response = serialPort.readAll();
//        qDebug() << response;
    }
}

void SensorInterface::readData()
{
    const QByteArray bytes = serialPort.readAll();
    data.append(bytes);
    parseData();
}

void SensorInterface::parseData()
{
    int idx = 0, found = 0;
    while (idx + 8 < data.size())
    {
        // Sync code
        if (data.at(idx) == 'S' && data.at(idx + 1) == 'T' && data.at(idx + 6) == 'E' && data.at(idx + 7) == 'D')
        {
            data.remove(0, 8);
            idx = 0;
            //idx += 8;
            //qDebug() << data.at(idx);
//            qDebug() << "Status Message";

            // Status Msg
        }

        // Data header
        if ((quint8)data.at(idx) == 0xce && (quint8)data.at(idx + 1) == 0xfa)
        {
            found = 1;
            break;
        }
        idx++;
    }

    // Non-data message
    if (idx > 0)// && found == 0)
    {
//        CommandData* cmd = new CommandData;
//        cmd->len = idx;
//        memcpy(cmd->body, data.constData(), cmd->len);

//        // Send to the main program
//        OnMsg(cmd);
    }

    while (found)
    {
        quint16 startAngle;
        quint16 count;

        memcpy(&count, data.constData() + idx + 2, 2); // Number of datapoints
        memcpy(&startAngle, data.constData()+ idx + 4, 2); // starting angle (0.1 deg)

        if (idx + 8 + count * 2 > data.size())  break;

        ScanData scanData;
        scanData.from = startAngle;
        scanData.span = 360;
        scanData.count = count;

        quint16 sum = count + startAngle;

        const char* pdata = data.constData() + idx + 6;
        for (int i = 0; i < count; i++)
        {
            quint8 lo_byte = pdata[i * 2];
            quint8 hi_byte = pdata[i * 2 + 1];

            quint16 val = (hi_byte << 8) + lo_byte;

            sum += val;

            scanData.dist[i] = val; //Distance (mm)
            scanData.intensity[i] = 0; // strength
        }

        quint8 lo = pdata[count * 2];
        quint8 hi = pdata[count * 2+1];
        quint16 chk = lo | (hi << 8);
        if (chk == sum)
        {
            // Emit Signal
//            qDebug() << "Emit! Count: " << count << ", Start: " << startAngle;
            newScan(scanData);
        }
        else
        {
            //printf("Checksum error\n");
        }

        idx += 8 + count * 2;
        data.remove(0, idx);
        idx = 0;

        if (idx + 8 > data.size()) break;

        if ((quint8)data.at(idx) == 0xce && (quint8)data.at(idx + 1) == 0xfa)
            found = 1;
        else
            found = 0;
    }
}

void SensorInterface::setDataSmoothing(bool enabled)
{
    if(!connected)
    {
        return;
    }

    if(enabled)
    {
        serialPort.write("LSSS1H");
    }
    else
    {
        serialPort.write("LSSS0H");
    }
}

void SensorInterface::setDragPointRemoval(bool enabled)
{
    if(!connected)
    {
        return;
    }

    if(enabled)
    {
        serialPort.write("LFFF1H");
    }
    else
    {
        serialPort.write("LFFF0H");
    }
}

