#ifndef SENSORINTERFACE_H
#define SENSORINTERFACE_H

#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <ScanData.h>

class SensorInterface : public QObject
{
    Q_OBJECT

public:
    SensorInterface(QObject *parent);
    QList<QSerialPortInfo> getSerialPorts();
    bool initialize(QString port, int baud);
    void start();
    void stop();
    void setDataSmoothing(bool enabled);
    void setDragPointRemoval(bool enabled);

private:
    void parseData();

    QSerialPort serialPort;
    QByteArray data;
    bool intensityMode;
    bool connected;

private slots:
    void readData();

signals:
    void newScan(ScanData &scan);
};

#endif // SENSORINTERFACE_H
